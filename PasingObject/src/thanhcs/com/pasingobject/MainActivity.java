package thanhcs.com.pasingobject;

import android.os.Bundle;
import android.app.Activity;
import android.widget.TextView;

public class MainActivity extends Activity {
	static{
		System.loadLibrary("jniFunction");
	}
	
	public native String getStringWelcome();
	public native int getCountElementOfObjectFromNative(ObjectData obj);
	public native float getSumElementOfObjectFromNative(ObjectData obj);
	public native ObjectData createObjectFromNative(int i);
	public native int processArrayObjectFromNative(ObjectData[] arrayObject);
	TextView tv, tv2;
	String text = "";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		tv = (TextView)findViewById(R.id.tv);
		
		text +=("\ngetStringWelcome : "+getStringWelcome());
		
		ObjectData obj =  new ObjectData(11);
		
		int result = getCountElementOfObjectFromNative(obj);
		float result2 = getSumElementOfObjectFromNative(obj);
		
		text+= ("\nCountElementOfObjectFromNative : "+result);
		text+= ("\nSumElementOfObjectFromNative : "+result2);
		
		ObjectData obj2  = createObjectFromNative(5);
		text+= ("\ncreateObjectFromNative: "+ obj2.toString());
		
		ObjectData[] obj3 = new ObjectData[2];
		obj3[0] =  new ObjectData(11);
		obj3[1] =  new ObjectData(22);
		int result3 = processArrayObjectFromNative(obj3);
	
		text+= ("\nprocessArrayObjectFromNative: " +result3);
		tv.setText(text);
		
		
	}
	
	

}
