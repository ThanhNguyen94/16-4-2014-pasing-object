package thanhcs.com.pasingobject;

public class ObjectData {

	private int numElement;

	public float[] floatArray;

	public ObjectData(int numElement) 
	{
		this.numElement = numElement;

		floatArray =  new float[numElement];

		for(int i=0; i<numElement; ++i)
		{         
			floatArray[i] = 10*i;
		}/**gan gia tri cho cac phan tu trong mang qua contructor*/
	}

	public int getnumElement() /**lay so phan tu cua mang*/
	{
		return numElement;
	}
	
	public String toString()
	{
		String text = "";
		for(int i = 0 ; i < numElement; ++i)
		{
			text += floatArray[i] + " | ";
		}
		return text;
	}


}
