#include "jni.h"

extern "C"
{
//Required to declare as extern "C" to prevent c++ compiler to mangle function names
JNIEXPORT
jstring
JNICALL
Java_thanhcs_com_pasingobject_MainActivity_getStringWelcome(
		JNIEnv *env,
		jobject callingObject)
{
	return env->NewStringUTF("Welcome");
}


int getCountElement(JNIEnv *env , jobject obj)
{
	jclass cls =  env->GetObjectClass(obj);
	jmethodID methodId = env->GetMethodID(cls, "getnumElement", "()I");
	int result =  env->CallIntMethod(obj, methodId);
	return result;
}


//Required to declare as extern "C" to prevent c++ compiler to mangle function names
JNIEXPORT
jint
JNICALL
Java_thanhcs_com_pasingobject_MainActivity_getCountElementOfObjectFromNative(
		JNIEnv *env,
		jobject callingObject,
		jobject obj)
{
	int result = getCountElement(env, obj);
	return result;
}


/**sum of element of my aray*/
JNIEXPORT
jfloat
JNICALL
Java_thanhcs_com_pasingobject_MainActivity_getSumElementOfObjectFromNative(
		JNIEnv *env,
		jobject callingObject,
		jobject obj)
{
	float result = 0.0f;

	jclass cls = env->GetObjectClass(obj); /**get object cua obj truyen vao , so cls <=> obj*/

	jfieldID fieldId = env->GetFieldID(cls, "floatArray", "[F");/**truy cap toi field du lieu cua object ten la flaotArray*/

	jobject objArray = env->GetObjectField (obj, fieldId);


	jfloatArray* fArray =  reinterpret_cast<jfloatArray*>(&objArray);

	jsize size = env->GetArrayLength(*fArray);

	float* data =  env->GetFloatArrayElements(*fArray, 0);

	for(int i = 0 ; i < size  ; i++ )

	{

		result += data[i];

	}

	env-> ReleaseFloatArrayElements(*fArray, data, 0);

	return result;
}


JNIEXPORT
jobject
JNICALL
Java_thanhcs_com_pasingobject_MainActivity_createObjectFromNative(
		JNIEnv *env,
		jobject callingObject,
		jint param)/**tham so truyen vao, tao doi tuong*/
{

	jclass cls =  env->FindClass("thanhcs/com/pasingobject/ObjectData");
	jmethodID methodId = env->GetMethodID(cls, "<init>",  "(I)V");
	jobject obj  = env->NewObject(cls, methodId, param);

	return obj;
}

JNIEXPORT
	jint
	JNICALL
	Java_thanhcs_com_pasingobject_MainActivity__processArrayObjectFromNative(
			JNIEnv *env,
			jobject callingObject,
			jobjectArray objArray)
	{
		int resultSum = 0;

		int size = env->GetArrayLength(objArray);

		for(int i=0; i<size; ++i)
		{
			jobject obj = (jobject) env->GetObjectArrayElement(objArray, i);
			resultSum = getCountElement(env, obj);
		}

		return resultSum;
	}
}








